package com.luxoft.patterns.creational.singleton;

/**
 * +thread-safe
 * +serializable
 * -not lazy loading
 */
public enum EnumSingleton {
    SINGLETON(1);

    private final int a;

    EnumSingleton(int a) {
        this.a = a;
    }
}
