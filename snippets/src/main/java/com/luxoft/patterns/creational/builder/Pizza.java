package com.luxoft.patterns.creational.builder;

/**
 * @author Alexander Kurochkin
 * <p>
 * Паттерн предлагает разбить процесс конструирования объекта на отдельные шаги, Причём не нужно запускать все шаги, а только те, что нужны
 * <p>
 * <p>
 * создать объект Дом. Чтобы построить стандартный дом, нужно поставить 4 стены, установить двери, вставить пару окон и постелить крышу.
 * Но что, если вы хотите дом побольше, посветлее, с бассейном, садом и прочим добром?
 * <p>
 * ...
 * builder vs huge constructor vs huge hierarchy of classes
 * * houseWithGarage
 * houseWithPool
 * HouseWithGarden
 *
 * можете создать несколько классов строителей, выполняющих одни и те же шаги по-разному
 * HouseFromStone
 *
 * Строитель не даёт доступа к результатам во время выполнения шагов конструирования. Это предотвращает клиентский код от получения незаконченных объектов.
 *
 * Director
 * Вы можете пойти дальше и выделить вызовы методов строителя в отдельный класс, называемый «Директором».
 * В этом случае директор будет задавать порядок шагов строительства, а строитель — выполнять их.
    директор полезен, если у вас есть несколько способов конструирования продуктов, отличающихся порядком и наличием шагов конструирования.

 *
 */
public class Pizza {
    private String cheese;

    public static class PizzaBuilder {
        private String cheese;

        public PizzaBuilder setCheese(String cheese) {
            this.cheese = cheese;
            return this;
        }

        public Pizza build() {
            Pizza pizza = new Pizza();
            pizza.cheese = cheese;
            return pizza;
        }
    }

}
