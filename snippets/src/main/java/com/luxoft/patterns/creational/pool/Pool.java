package com.luxoft.patterns.creational.pool;

/**
 * @author Alexander Kurochkin
 */
public interface Pool<T> {

    T aquire();

    void release(T t);


}
