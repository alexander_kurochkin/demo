package com.luxoft.patterns.creational.factory;

/**
 *
 */
public class ConcreteFactory implements AbstractFactory {
    @Override
    public Spoon getSpoon() {
        return new ConcreteSpoon();  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public Knife getKnife() {
        return new ConcreteKnife();  //To change body of implemented methods use File | Settings | File Templates.
    }
}
