package com.luxoft.patterns.creational.factory;

/**
 *
 */
public interface AbstractFactory {


    Spoon getSpoon();

    Knife getKnife();
}
