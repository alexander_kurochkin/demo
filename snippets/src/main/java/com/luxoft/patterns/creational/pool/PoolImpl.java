package com.luxoft.patterns.creational.pool;

import com.luxoft.patterns.creational.factory.FactoryMethod;
import com.luxoft.patterns.creational.factory.Spoon;

/**
 * @author Alexander Kurochkin
 */
public class PoolImpl extends PoolAbstract<Spoon> {

    private final FactoryMethod factory;

    public PoolImpl(FactoryMethod factory) {this.factory = factory;}

    @Override
    public Spoon create() {
        return factory.createSpoon();
    }
}
