package com.luxoft.patterns.creational.singleton;

/**
 * On Demand holder , uses class loader features
 * <p>
 * we put parent singleton static instance into static class
 * + Lazy initialization
 * + High performance
 * - Can not be used for non-static class fields
 */
public class OnDemandHolderSingleton {

    private static class InnerClass {
        private static final OnDemandHolderSingleton instance = new OnDemandHolderSingleton();
    }

    public static OnDemandHolderSingleton getInstance() {
        return InnerClass.instance;
    }

    private OnDemandHolderSingleton() {
    }
}
