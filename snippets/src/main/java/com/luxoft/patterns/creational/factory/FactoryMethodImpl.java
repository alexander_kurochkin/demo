package com.luxoft.patterns.creational.factory;

/**
 *
 */
public class FactoryMethodImpl implements FactoryMethod {
    @Override
    public Spoon createSpoon() {
        return new ConcreteSpoon();  //To change body of implemented methods use File | Settings | File Templates.
    }
}
