package com.luxoft.patterns.creational.pool;

import com.luxoft.patterns.creational.factory.FactoryMethodImpl;
import com.luxoft.patterns.creational.factory.Spoon;

/**
 * @author Alexander Kurochkin
 */
public class Client {

    public static void main(String[] args) {

        Pool<Spoon> pool = new PoolImpl(new FactoryMethodImpl());

        Spoon spoon = pool.aquire();
        pool.release(spoon);

    }

}
