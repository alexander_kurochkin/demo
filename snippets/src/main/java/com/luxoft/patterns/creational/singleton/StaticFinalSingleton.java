package com.luxoft.patterns.creational.singleton;


public class StaticFinalSingleton {
    private static final StaticFinalSingleton ourInstance = new StaticFinalSingleton();

    public static StaticFinalSingleton getInstance() {
        return ourInstance;
    }

    private StaticFinalSingleton() {
    }
}
