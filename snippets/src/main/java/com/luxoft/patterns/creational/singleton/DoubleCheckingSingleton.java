package com.luxoft.patterns.creational.singleton;

/**
 * гарантирует, что у класса есть только один экземпляр, и предоставляет к нему глобальную точку доступа.
 * <p>
 * +ThreadSafe
 * +LazyLoading
 * + performance call
 */
public class DoubleCheckingSingleton {

    private static volatile DoubleCheckingSingleton instance;

    /**
     * to reduce the overhead associated with obtaining a lock.
     * First, the initialization is checked, without any synchronization; only then the thread tries to get a lock
     * <p>
     * // why instance is volatile // to guaranty visibility : happens-before
     * A: its because of Java Memory Model
     * 1) local_ptr = malloc(sizeof(Student)) // allocating memory for the object itself;
     * 2) s = local_ptr // initialization of pointer
     * between 3rd and 2nd step you can get trash, to defence against it we use volatile
     * 3) Student::ctor(s); // constructing the object (initializing fields);
     *
     * @return
     */
    public static DoubleCheckingSingleton getInstance() {

        if (instance == null) {  // why instance is volatile // only volatile have thread-safe guarantee for initialization
            //race can be here //two threads can pass this condition
            synchronized (DoubleCheckingSingleton.class) {
                if (instance == null) { //visibility thread safe - because volatile
                    instance = new DoubleCheckingSingleton();
                }
                return instance;
            }
        }

        return instance;
    }

}
