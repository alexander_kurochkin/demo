package com.luxoft.patterns.creational.factory;

/**
 *
 */
public interface FactoryMethod {

    Spoon createSpoon();

}
