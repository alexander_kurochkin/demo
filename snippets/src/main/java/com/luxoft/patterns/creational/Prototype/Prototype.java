package com.luxoft.patterns.creational.Prototype;

import java.util.Date;

/**
 * позволяет копировать объекты, не вдаваясь в подробности их реализации.
 * Паттерн Прототип поручает создание копий самим копируемым объектам.
 * <p>
 *
 * Он вводит общий интерфейс для всех объектов, поддерживающих клонирование
 * Тк не каждый объект удастся скопировать таким образом, ведь часть его состояния может быть приватной и недоступна для остального кода программы.
 * <p>
 * если создание объектов (через оператор new) занимает длительный промежуток времени или требовательно к памяти;
 * если создание объектов для клиента является нетривиальной задачей, например, когда объект составной;
 * избежать множества фабрик для создания конкретных экземпляров классов;
 * если клиент не знает специфики создания объекта.
 * В Java уже заложена функциональность для имплементации паттерна Прототип - интерфейс Cloneable.
 */
public class Prototype implements Cloneable {

    private String id;

    private Date date;

    public Prototype(String id, Date date) {
        this.id = id;
        this.date = date;
    }

    public Object clone() throws CloneNotSupportedException {
        return new Prototype(id, new Date(date.getTime()));
    }

}
