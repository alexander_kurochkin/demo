package com.luxoft.patterns.creational.pool;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Alexander Kurochkin
 * <p>
 * <p>
 * https://github.com/iluwatar/java-design-patterns/blob/master/object-pool/src/main/java/com/iluwatar/object/pool/ObjectPool.java
 */
public abstract class PoolAbstract<T> implements Pool<T> {

    Set<T> available = new HashSet<>();
    Set<T> inUse = new HashSet<>();
    private int maxPoolSize;

    @Override
    public synchronized T aquire() {

        if (available.isEmpty()) {
            available.add(create());
        }

        T next = available.iterator().next();
        available.remove(next);
        inUse.add(next);

        return next;
    }

    @Override
    public synchronized void release(T t) {
        inUse.remove(t);
        available.add(t);
    }



    public abstract T create();

}
