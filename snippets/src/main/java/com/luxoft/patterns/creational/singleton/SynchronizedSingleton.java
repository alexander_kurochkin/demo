package com.luxoft.patterns.creational.singleton;

/**
 * +ThreadSafe
 * +LazyLoading
 * - performance call
 */
public class SynchronizedSingleton {


    private static SynchronizedSingleton instance;

    public static synchronized SynchronizedSingleton getInstance() {
        if (instance == null) {
            instance = new SynchronizedSingleton();
        }

        return instance;
    }

    private SynchronizedSingleton() {

    }
}
