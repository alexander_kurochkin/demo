package com.luxoft.patterns.other.callback;

/**
 * @author Alexander Kurochkin
 */
public class ServiceImpl implements Service {
    private Callback callback;

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public void doJob() {
        System.out.println("doJob");
        if (callback != null) {
            callback.call();
        }
    }

    @Override
    public void doJob(Callback callback) {
        System.out.println("doJob");
        if (callback != null) {
            callback.call();
        }
    }
}
