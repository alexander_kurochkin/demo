package com.luxoft.patterns.other.callback;

/**
 * @author Alexander Kurochkin
 */
public class CallbackImpl implements Callback {
    @Override
    public void call() {
        System.out.println("callback");
    }
}
