package com.luxoft.patterns.other.callback;

/**
 * @author Alexander Kurochkin
 */
public interface Service {
    void doJob();

    void doJob(Callback callback);
}
