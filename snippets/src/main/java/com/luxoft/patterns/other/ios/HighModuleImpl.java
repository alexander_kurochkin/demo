package com.luxoft.patterns.other.ios;

/**
 * @author Alexander Kurochkin
 * <p>
 * implements inversion of control. It depends on abstraction that can be injected
 * through its setter.
 * <p>
 * do not depend on any concrete implementation but abstraction.
 * <p>
 * dependency to be injected through constructor  or setter  .
 * This way, handling the dependency is no longer the module responsibility. It is resolved outside the wizard class.
 */
public class HighModuleImpl implements HighModule {

    private Dependency dep;

    @Override
    public void work() {
        dep.method();
    }

    public void setDep(Dependency dep) {
        this.dep = dep;
    }
}
