package com.luxoft.patterns.other.callback;

/**
 * @author Alexander Kurochkin
 * <p>
 * callback does not do anything relevant to the object's behavior: it just notifies the caller in some way
 */
public interface Callback {

    void call();
}
