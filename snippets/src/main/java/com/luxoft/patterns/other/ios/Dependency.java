package com.luxoft.patterns.other.ios;

/**
 * @author Alexander Kurochkin
 */
public interface Dependency {
    void method();
}
