package com.luxoft.patterns.other.ios;

/**
 * @author Alexander Kurochkin
 * <p>
 * Dependency Injection pattern deals with how objects handle their dependencies. The pattern
 * implements so called inversion of control principle. Inversion of control has two specific rules:
 * - High-level modules should not depend on low-level modules. Both should depend on abstractions.
 * - Abstractions should not depend on details. Details should depend on abstractions.
 */
public interface HighModule {

    void work();
}
