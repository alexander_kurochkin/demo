package com.luxoft.patterns.other.filterOrCritera;

import java.util.List;

public interface  Criteria<T> {
    List<T> meetCriteria(List<T> persons);
}
