package com.luxoft.patterns.structural.Composite;

import java.util.List;

public interface Node1 {
    List<Node1> getChildren();

    void addChild(Node1 node);

    int getWeight();

    int getTotalWeight();
}
