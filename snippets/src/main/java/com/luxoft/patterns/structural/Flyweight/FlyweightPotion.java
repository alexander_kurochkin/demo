package com.luxoft.patterns.structural.Flyweight;

/**
 * @author Alexander Kurochkin
 * <p>
 * <p>
 * Flyweight pattern is primarily used to reduce the number of objects created
 * and to decrease memory usage and increase performance.
 * <p/>
 * tries to reuse already existing similar kind objects by storing them
 * and creates new object when no matching object is found.
 * <p>
 * позволяет вместить бо́льшее количество объектов в отведённую оперативной память
 * за счёт экономного разделения общего состояния объектов между собой, вместо хранения одинаковых данных в каждом объекте.
 * <p>
 * они хранятся в каждом объекте, хотя фактически их значения одинаковые для большинства частиц.
 * <p>
 * Паттерн Легковес предлагает не хранить в классе одинаковое состояние,
 * а передавать его в те или иные методы через параметры. Таким образом, одни и те же объекты можно будет повторно использовать в различных контекстах.
 */
public interface FlyweightPotion {
    void drink();
}
