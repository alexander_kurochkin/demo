package com.luxoft.patterns.structural.facade;

/**
 * @author Alexander Kurochkin
 * <p>
 * Фасад — это простой интерфейс работы со сложной подсистемой
 * <p>
 * Фасад полезен, если вы используете какую-то сложную библиотеку со множеством подвижных частей, но вам нужна только часть её возможностей.
 */
public class Facade {

    private Subsystem1 subsystem1;
    private Subsystem2 subsystem2;
    private Subsystem3 subsystem3;

    public void work() {
        subsystem1.prepare();
        subsystem2.work();
        subsystem3.clean();
    }

}
