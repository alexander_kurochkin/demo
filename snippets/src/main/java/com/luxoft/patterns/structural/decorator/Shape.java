package com.luxoft.patterns.structural.decorator;

/**
 * Created by akurochk on 4/10/15.
 */
public interface Shape {

    void resize();
}
