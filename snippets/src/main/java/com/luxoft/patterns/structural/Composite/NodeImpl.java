package com.luxoft.patterns.structural.Composite;

import java.util.ArrayList;
import java.util.List;

/**
 * Composite   composes objects in term of a tree structure to represent hierarchy
 * <p/>
 * provides ways to work with all objects in hierarchy
 *
 * позволяет сгруппировать объекты в древовидную структуру, а затем работать с ними так, если бы это был единичный объект.
 * ArmyUnit  (for each child move)
 * Node
 *
 */
public class NodeImpl implements Node1 {

    private int weight;
    private List<Node1> childs = new ArrayList<>();

    public NodeImpl(int weight) {
        this.weight = weight;
    }

    @Override
    public List<Node1> getChildren() {
        return childs;
    }

    @Override
    public void addChild(Node1 node) {
        childs.add(node);
    }

    @Override
    public int getWeight() {
        return weight;
    }

    @Override
    public int getTotalWeight() {

        int totalWeight = this.weight;

        for (Node1 node1 : childs) {
            totalWeight += node1.getWeight();
        }

        return totalWeight;
    }

}
