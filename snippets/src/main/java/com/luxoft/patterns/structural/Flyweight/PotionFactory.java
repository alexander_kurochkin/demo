package com.luxoft.patterns.structural.Flyweight;

import java.util.EnumMap;
import java.util.Map;

/**
 * @author Alexander Kurochkin
 */
public class PotionFactory {

    Map<PotionType, FlyweightPotion> map = new EnumMap<>(PotionType.class);

    public FlyweightPotion createPotion(PotionType type) {

        switch (type) {

            case FLY:
                return map.computeIfAbsent(PotionType.FLY, e -> new FlyPotion());
            case ARMOR:
                return map.computeIfAbsent(PotionType.ARMOR, e -> new ArmorPotion());
            case HEAL:
                return map.computeIfAbsent(PotionType.FLY, e -> new HealPotion());

            default:
                throw new IllegalArgumentException("illegal" + type);

        }

    }

}
