package com.luxoft.patterns.structural.proxy;

/**
 *
 * proxy implements an interface for the purpose of providing access to something else, like lazy access, security access,  
 *
 * объекты перехватывают вызовы к оригинальному объекту, позволяя сделать что-то до или после передачи вызова оригиналу.
 * <p>
 * оместить в класс заместителя какую-то промежуточную логику, которая выполнялась бы до (или после) вызовов этих же методов
 * <p>
 *
 *     ленивой инициализации и
 *     кеширования(одинаковое видео)
 *     security
 *     logging
 *     transaction
 *     remote proxy
 *     
 *
 * Банковский чек — это заместитель пачки наличности.
 */
public class Proxy {

    public static class Circle {

        public void draw() {

        }

    }

    public static class CircleProxy extends Circle {

        private Circle circle;

        public void draw() {
            if (circle == null) {
                circle = new Circle();
            }
            circle.draw();
        }

    }

}
