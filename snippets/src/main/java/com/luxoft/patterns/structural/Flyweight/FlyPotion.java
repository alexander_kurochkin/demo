package com.luxoft.patterns.structural.Flyweight;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Alexander Kurochkin
 */
public class FlyPotion implements FlyweightPotion {
    private final static Logger LOG = LoggerFactory.getLogger(FlyPotion.class);

    public FlyPotion() {
    }


    @Override
    public void drink() {
        LOG.info("flying");

    }
}
