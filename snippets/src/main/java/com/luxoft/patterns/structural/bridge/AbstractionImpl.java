package com.luxoft.patterns.structural.bridge;

public class AbstractionImpl {

    private Realization realization;

    public AbstractionImpl(Realization realization) {

        this.realization = realization;
    }

    public void abstractWork() {
        realization.realizationMethod();
    }


}
