package com.luxoft.patterns.structural.decorator;

/**
 * Decorator class implements the component interface and it has a HAS-A relationship with the component interface.
 */
public interface ShapeDecorator extends Shape {

    void draw();
}
