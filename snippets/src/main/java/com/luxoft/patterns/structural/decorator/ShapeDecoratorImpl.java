package com.luxoft.patterns.structural.decorator;

/**
 * @author Alexander Kurochkin
 * <p>
 * Декоратор — это структурный паттерн проектирования, который позволяет динамически добавлять объектам новую функциональность, оборачивая их в полезные «обёртки».
 * расширяет базовое поведение объекта.
 * Декораторы, как и сам класс данных, имеют общий интерфейс, плюс ф-ть декоратора
 */
public class ShapeDecoratorImpl implements ShapeDecorator {

    private final Shape shape;

    public ShapeDecoratorImpl(Shape shape) {
        this.shape = shape;
    }

    @Override
    public void draw() {
        //decorator method
    }

    @Override
    public void resize() {
        shape.resize();
    }

}
