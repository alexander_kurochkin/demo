package com.luxoft.patterns.structural.Flyweight;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Alexander Kurochkin
 */
public class ArmorPotion implements FlyweightPotion {
    private final static Logger LOG = LoggerFactory.getLogger(ArmorPotion.class);

    @Override
    public void drink() {
        LOG.info("armed");
    }
}
