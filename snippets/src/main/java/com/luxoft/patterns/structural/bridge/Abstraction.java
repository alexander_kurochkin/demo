package com.luxoft.patterns.structural.bridge;

/**
 * A bridge is a structural pattern that separates business logic or a large class into several separate hierarchies,
 * which can then be developed separately from each other.
 * <p>
 * Bridge prefer composition over inheritance
 * <p>
 * <p>
 * <p>
 * One of these hierarchies (abstraction) will get a reference to objects of another hierarchy (implementation)
 * and will delegate them the main charge.
 * Due to the fact that all implementations follow the common interface, they can be interchangeable within an abstraction.
 * <p>
 * example
 * The remote controller act as an "abstraction", and the devices - "implementation". The same devices can work with different consoles, and consoles control different devices.
 */
public interface Abstraction {

    void setRealization(Realization realization);

    void abstractionMethod();
}
