package com.luxoft.patterns.structural.Flyweight;

/**
 * @author Alexander Kurochkin
 */
public enum PotionType {
    HEAL, FLY, ARMOR
}
