package com.luxoft.patterns.structural.bridge;

/**
 * Created by alex on 23.08.2017.
 */
public interface Realization {

    void realizationMethod();

}
