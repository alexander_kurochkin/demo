package com.luxoft.patterns.structural.adapter;

/**
 * @author Alexander Kurochkin
 */
public class EuToUsAdapter {

    private UsSocket usSocket;

    public EuToUsAdapter(UsSocket s) {
        usSocket = s;
    }

    void charge(EuPlug euPlug) {
        usSocket.charge(convert(euPlug));
    }

    private UsPlug convert(EuPlug euPlug) {
        return new UsPlug();
    }

}
