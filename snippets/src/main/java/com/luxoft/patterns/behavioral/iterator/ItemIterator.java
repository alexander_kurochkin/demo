package com.luxoft.patterns.behavioral.iterator;

/**
 * @author Alexander Kurochkin
 */
public interface ItemIterator {

    boolean hasNext();
    Item next();

}
