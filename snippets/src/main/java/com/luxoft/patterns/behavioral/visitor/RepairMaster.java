package com.luxoft.patterns.behavioral.visitor;

/**
 *  execution algorithm of element can vary as and when visitor varies.
 */
public class RepairMaster implements Visitor {
    @Override
    public void visit(Car car) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void visit(Toyota car) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public void visit(Track car) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
