package com.luxoft.patterns.behavioral.Command;

import java.util.Arrays;
import java.util.List;

/**
 *
 */
public class Client {

    private Broker broker = new Broker();

    public static void main(String[] args) {
        new Client().sendOrders();
    }

    private void sendOrders() {

        List<Order> orders = Arrays.asList(new SellOrder(), new BuyOrder());

        broker.processOrders(orders);
    }
}
