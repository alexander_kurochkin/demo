package com.luxoft.patterns.behavioral.iterator;

/**
 * @author Alexander Kurochkin
 */
public class Client {

    public static void main(String[] args) {
        ItemCollection itemCollection = new ItemCollectionImpl();

        itemCollection.addItem(new Item("1"));
        itemCollection.addItem(new Item("2"));
        itemCollection.addItem(new Item("3"));

        ItemIterator iterator = itemCollection.iterator();

        while (iterator.hasNext()) {
            Item next = iterator.next();
            System.out.println(next);
        }

    }

}
