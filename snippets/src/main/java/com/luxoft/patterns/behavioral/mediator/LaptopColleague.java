package com.luxoft.patterns.behavioral.mediator;


public class LaptopColleague extends Colleague
{
    public LaptopColleague(Mediator m) {
        super(m);
    }

    public void receive(String message)
    {
        System.out.println("Colleague Received: " + message);
    }

}
