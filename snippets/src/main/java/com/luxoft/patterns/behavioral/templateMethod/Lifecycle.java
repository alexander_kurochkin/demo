package com.luxoft.patterns.behavioral.templateMethod;

/**
 * @author Alexander Kurochkin
 */
public interface Lifecycle {
    void runTemplateMethod(Service service);
}
