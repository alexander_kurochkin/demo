package com.luxoft.patterns.behavioral.Memento;

/**
 * @author Alexander Kurochkin
 * <p>
 * The Memento pattern retrieves and stores its internal state outside the object so that it can later be reconstructed in the same state.
 * <p>
 * this state is accessible only to this object
 * <p>
 * позволяет делать снимки состояния объектов, не раскрывая подробностей их реализации. Затем снимки можно использовать, чтобы восстановить прошлое состояние объектов.
 */
public class MementoParent {

    private int state;

    public class Memento {
        private int state;

        public Memento(int state) {

            this.state = state;
        }

        private int getState() {
            return state;
        }
    }

    public Memento getState() {
        return new Memento(state);
    }

    public void setState(Memento state) {
        this.state = state.getState();
    }
}
