package com.luxoft.patterns.behavioral.Memento;

/**
 * Created by alex on 12.09.2017.
 */
public class MementoClient {
    public static void main(String[] args) {
        MementoParent mementoParent = new MementoParent();

        MementoParent.Memento state = mementoParent.getState();
        mementoParent.setState(state);

    }
}
