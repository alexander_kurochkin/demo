package com.luxoft.patterns.behavioral.interpreter;

/**
 * @author Alexander Kurochkin
 */
public class NumberExpression implements Expression {

    private int number;

    public NumberExpression(String number) {
        this.number = Integer.parseInt(number);
    }

    public NumberExpression(int number) {
        this.number = number;
    }

    @Override
    public int intrerprent() {
        return number;
    }
}
