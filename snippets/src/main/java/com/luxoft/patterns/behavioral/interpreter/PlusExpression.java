package com.luxoft.patterns.behavioral.interpreter;

/**
 * @author Alexander Kurochkin
 */
public class PlusExpression implements Expression {

    private Expression left;
    private Expression rigth;

    public PlusExpression(Expression left, Expression rigth) {
        this.left = left;
        this.rigth = rigth;
    }

    @Override
    public int intrerprent() {
        return left.intrerprent() + rigth.intrerprent();
    }
}
