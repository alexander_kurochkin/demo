package com.luxoft.patterns.behavioral.strategy;

/**
 * @author Alexander Kurochkin
 */
public interface WorkStrategy {
    void work();
}
