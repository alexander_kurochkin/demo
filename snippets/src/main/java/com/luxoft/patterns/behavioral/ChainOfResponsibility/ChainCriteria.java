package com.luxoft.patterns.behavioral.ChainOfResponsibility;

public enum ChainCriteria {
    INFO, WARN, ERROR
}
