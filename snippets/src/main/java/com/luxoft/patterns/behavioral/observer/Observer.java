package com.luxoft.patterns.behavioral.observer;

/**
 * Created by alex on 23.08.2017.
 */
public interface Observer {
    <T> void onNotify(T notifyObject);
}
