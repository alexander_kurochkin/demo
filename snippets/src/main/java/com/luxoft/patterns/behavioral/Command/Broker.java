package com.luxoft.patterns.behavioral.Command;

import java.util.List;

/**
 *
 */
public class Broker {

    public void processOrders(List<Order> orders) {
        for (Order order : orders) {
            order.execute();
        }

    }

}
