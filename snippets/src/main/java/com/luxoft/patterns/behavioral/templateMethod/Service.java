package com.luxoft.patterns.behavioral.templateMethod;


;

/**
 * @author Alexander Kurochkin
 */
public  interface Service<T>  {
    void init();
    void service(T argument);
    void destroy();
}
