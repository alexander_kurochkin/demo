package com.luxoft.patterns.behavioral.iterator;

/**
 * @author Alexander Kurochkin
 */
public class ItemIteratorImpl implements ItemIterator {

    private int position = 0;
    private ItemCollection itemCollection;

    public ItemIteratorImpl(ItemCollection itemCollection) {

        this.itemCollection = itemCollection;
    }

    @Override
    public boolean hasNext() {
        return itemCollection.size() > position;
    }

    @Override
    public Item next() {
        return itemCollection.getItem(position++);
    }
}
