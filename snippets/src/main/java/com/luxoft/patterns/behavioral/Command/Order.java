package com.luxoft.patterns.behavioral.Command;

/**
 *
 */
public interface Order {

    void execute();
}
