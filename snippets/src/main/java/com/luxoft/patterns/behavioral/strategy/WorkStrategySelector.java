package com.luxoft.patterns.behavioral.strategy;

/**
 * @author Alexander Kurochkin
 */
public interface WorkStrategySelector {
    WorkStrategy selectStrategy();
}
