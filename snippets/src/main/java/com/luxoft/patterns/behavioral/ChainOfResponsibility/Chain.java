package com.luxoft.patterns.behavioral.ChainOfResponsibility;

public interface Chain {

    void setNext(Chain chain);

    void work(ChainCriteria chainCriteria);
}
