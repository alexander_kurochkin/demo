package com.luxoft.patterns.behavioral.state;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Alexander Kurochkin
 */
public class PendingState implements State {

    private final Transaction transaction;

    private final static Logger LOG = LoggerFactory.getLogger(PendingState.class);

    public PendingState(Transaction transaction) {
        this.transaction = transaction;
    }

    @Override
    public void onEnterState(BalanceCalculator balanceCalculator) {
        LOG.info("{} pending", transaction);

    }
}
