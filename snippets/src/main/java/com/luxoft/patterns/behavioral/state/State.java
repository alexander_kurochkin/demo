package com.luxoft.patterns.behavioral.state;

/**
 * @author Alexander Kurochkin
 * <p>
 * State pattern -behaviour
 * <p>
 * Allow an object to alter its behavior when its internal state changes.
 * an object's behavior depends on its state, and it must change its behavior depending on that state
 * <p>
 * operations have large, conditional statements that depend on the object's state.
 * The State pattern puts each branch of conditional behaviour in a separate class.
 * <p>
 * incapsulate state, and logic that depends on concrete state
 */
public interface State {

    /**
     * behaviour depends on concrete state
     *
     * @param balanceCalculator
     */
    void onEnterState(BalanceCalculator balanceCalculator);
}
