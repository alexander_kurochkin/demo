package com.luxoft.patterns.behavioral.visitor;

/**
 *
 */
public class Toyota implements Car {
    @Override
    public void accept(Visitor visitor) {
        visitor.visit(this );
    }
}
