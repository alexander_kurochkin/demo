package com.luxoft.patterns.behavioral.templateMethod;

/**
 * @author Alexander Kurochkin
 */
public class ServiceImpl2<T> extends ServiceTemplate<T> {
    @Override
    public void service(T argument) {
        System.out.println("doWork2");
    }
}
