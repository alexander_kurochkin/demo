package com.luxoft.patterns.behavioral.ChainOfResponsibility;

import static com.luxoft.patterns.behavioral.ChainOfResponsibility.ChainCriteria.ERROR;
import static com.luxoft.patterns.behavioral.ChainOfResponsibility.ChainCriteria.INFO;

public class ChainOfResponsibility {
    public static Chain getChain() {
        Chain chain1 = new Chain1();
        Chain chain2 = new Chain2();
        chain1.setNext(chain2);

        return chain1;
    }

    public static void main(String[] args) {
        Chain chain = getChain();

        chain.work(ERROR);
        chain.work(INFO);
    }
}
