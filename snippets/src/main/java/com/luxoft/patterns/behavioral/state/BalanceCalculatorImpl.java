package com.luxoft.patterns.behavioral.state;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Alexander Kurochkin
 *
 *
 */
public class BalanceCalculatorImpl implements BalanceCalculator {

    private final static Logger LOG = LoggerFactory.getLogger(BalanceCalculatorImpl.class);

    @Override
    public void approve(Transaction transaction) {
        State state = transaction.getState();
        if (state.getClass().equals(PendingState.class)) {
            changeStateTo(transaction, new ApprovedState(transaction));
        } else {
            throw new InvalidStateException("invalid state to approve ", state);
        }
    }

    private void changeStateTo(Transaction transaction, State state) {
        state.onEnterState(this);
        transaction.setState(state);
    }

    @Override
    public void increaseBalance(Transaction transaction) {
        LOG.info("increaseBalance");

    }
}
