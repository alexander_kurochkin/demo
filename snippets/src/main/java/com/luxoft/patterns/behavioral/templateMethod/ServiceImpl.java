package com.luxoft.patterns.behavioral.templateMethod;

/**
 * @author Alexander Kurochkin
 */
public class ServiceImpl<T> extends ServiceTemplate<T> {
    @Override
    public void service(T argument) {
        System.out.println("doWork1");
    }
}
