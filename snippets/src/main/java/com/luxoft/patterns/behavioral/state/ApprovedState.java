package com.luxoft.patterns.behavioral.state;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Alexander Kurochkin
 */
public class ApprovedState implements State {

    private final Transaction transaction;

    private final static Logger LOG = LoggerFactory.getLogger(ApprovedState.class);

    public ApprovedState(Transaction transaction) {
        this.transaction = transaction;
    }


    @Override
    public void onEnterState(BalanceCalculator balanceCalculator) {
        balanceCalculator.increaseBalance(transaction);
        LOG.info("{} approved", transaction);
    }
}
