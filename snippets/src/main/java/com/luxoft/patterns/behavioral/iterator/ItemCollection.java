package com.luxoft.patterns.behavioral.iterator;

/**
 * @author Alexander Kurochkin
 */
public interface ItemCollection {

    boolean addItem(Item item);

    Item getItem(int index);

    int size();

    boolean removeItem(Item item);

    ItemIterator iterator();
}
