package com.luxoft.patterns.behavioral.interpreter;

/**
 * @author Alexander Kurochkin
 * <p>
 * The Expression pattern is a design pattern that specifies how to evaluate sentences in a
 * language. The basic idea is to have a class for each symbol (terminal or nonterminal) in a
 * specialized computer language. The syntax tree of a sentence in the language is an instance of
 * the composite pattern and is used to evaluate (interpret) the sentence for a client.
 * <p>
 * also known as Little (Small) Language.
 * <p>
 * <p>
 * lets see on example how to calculate  : Reverse Polish Notation (Postfix)
 * In reverse polish notation (postfix), the operator comes after the operand. Example 1 2 +
 */
public interface Expression {

    int intrerprent();
}
