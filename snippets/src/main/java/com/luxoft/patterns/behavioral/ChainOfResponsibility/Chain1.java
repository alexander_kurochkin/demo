package com.luxoft.patterns.behavioral.ChainOfResponsibility;

import static com.luxoft.patterns.behavioral.ChainOfResponsibility.ChainCriteria.ERROR;

public class Chain1 extends ChainCommon {

    @Override
    protected boolean workCriteria(ChainCriteria chainCriteria) {
        return chainCriteria == ERROR;
    }

    @Override
    protected void work() {
        System.out.println(Chain2.class.getName());
    }

}
