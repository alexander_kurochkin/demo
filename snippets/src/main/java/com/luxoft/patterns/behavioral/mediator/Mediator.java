package com.luxoft.patterns.behavioral.mediator;

/**
 * mediator pattern centralizes communication between objects into a mediator object.
 * This centralization is useful since it localizes in one place the interactions between objects,
 * which can increase code maintainability,
 *
 * communication occurs with the mediator rather than directly with other objects,
 * the mediator pattern results in a loose coupling of objects.
 *
 */
public interface Mediator
{
    void send(String message, Colleague colleague);
}