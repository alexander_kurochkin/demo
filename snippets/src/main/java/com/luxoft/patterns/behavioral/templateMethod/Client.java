package com.luxoft.patterns.behavioral.templateMethod;

/**
 * @author Alexander Kurochkin
 */
public class Client {
    public static void main(String[] args) {

        LifecycleImpl lifecycle = new LifecycleImpl();
        
        lifecycle.runTemplateMethod(new ServiceImpl());
        lifecycle.runTemplateMethod(new ServiceImpl2());


    }
}
