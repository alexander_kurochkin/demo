package com.luxoft.patterns.behavioral.iterator;

/**
 * @author Alexander Kurochkin
 */
public class Item {

    private String field;

    public Item(String field) {

        this.field = field;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    @Override
    public String toString() {
        return "Item{" +
                "field='" + field + '\'' +
                '}';
    }
}
