package com.luxoft.patterns.behavioral.strategy;

/**
 * @author Alexander Kurochkin
 */
public class StrategyClient {

    private final WorkStrategySelectorImpl workStrategySelector;

    public StrategyClient(WorkStrategySelectorImpl workStrategySelector) {this.workStrategySelector = workStrategySelector;}

    public void work() {
        WorkStrategy workStrategy = workStrategySelector.selectStrategy();
        workStrategy.work();
    }

}
