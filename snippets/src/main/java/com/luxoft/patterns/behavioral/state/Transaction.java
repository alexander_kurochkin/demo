package com.luxoft.patterns.behavioral.state;

import java.math.BigDecimal;

/**
 * @author Alexander Kurochkin
 */
public class Transaction {

    private String account;
    private BigDecimal amount;

    private State state;

    public Transaction() {
        this.state = new PendingState(this);
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}
