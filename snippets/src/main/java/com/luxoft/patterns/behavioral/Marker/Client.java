package com.luxoft.patterns.behavioral.Marker;

/**
 * @author Alexander Kurochkin
 */
public class Client {

    public static void main(String[] args) {
        Client c = new Client();
        c.check(new Guard());
        c.check(new OtherPerson());

    }

    public boolean check(Object o) {

        if (o instanceof Permission) {
            return true;
        } else {
            return false;
        }
    }

}
 
