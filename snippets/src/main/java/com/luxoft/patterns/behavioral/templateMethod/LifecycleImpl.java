package com.luxoft.patterns.behavioral.templateMethod;

/**
 * @author Alexander Kurochkin
 */
public class LifecycleImpl implements Lifecycle {
    @Override
    public void runTemplateMethod(Service service) {
        service.init();
        service.service("");
        service.destroy();
    }
}
