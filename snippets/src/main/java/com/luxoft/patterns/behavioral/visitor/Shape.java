package com.luxoft.patterns.behavioral.visitor;

/**
 * @author Alexander Kurochkin
 */
public interface Shape {
    void accept(Visitor visitor);
}
