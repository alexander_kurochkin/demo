package com.luxoft.patterns.behavioral.iterator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Alexander Kurochkin
 */
public class ItemCollectionImpl implements ItemCollection {

    List<Item> list = new ArrayList<>();

    @Override
    public boolean addItem(Item item) {
        return list.add(item);
    }

    @Override
    public Item getItem(int index) {
        return list.get(index);
    }

    @Override
    public int size() {
        return list.size();
    }

    @Override
    public boolean removeItem(Item item) {
        return list.remove(item);
    }

    @Override
    public ItemIterator iterator() {
        return new ItemIteratorImpl(this) {};
    }
}
