package com.luxoft.patterns.behavioral.visitor;

/**
 *
 */
public interface Visitor {

    void visit(Car car);

    void visit(Toyota car);

    void visit(Track car);

}
