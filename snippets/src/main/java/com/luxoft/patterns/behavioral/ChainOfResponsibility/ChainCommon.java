package com.luxoft.patterns.behavioral.ChainOfResponsibility;

public abstract class ChainCommon implements Chain {

    private Chain next;

    @Override
    public void setNext(Chain chain) {
        this.next = chain;
    }

    @Override
    public void work(ChainCriteria chainCriteria) {
        if (workCriteria(chainCriteria)) {
            work();
        }
        if (next != null) {
            next.work(chainCriteria);
        }

    }

    protected abstract boolean workCriteria(ChainCriteria chainCriteria);

    protected abstract void work();
}
