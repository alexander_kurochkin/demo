package com.luxoft.patterns.behavioral.strategy;

public class WorkStrategySelectorImpl implements WorkStrategySelector {
    public WorkStrategySelectorImpl() { }

    @Override
    public WorkStrategy selectStrategy() {
        return new SmartWorkStrategyImpl();
    }
}