package com.luxoft.patterns.behavioral.mediator;

public class Client {
    public static void main(String[] args) {
        ApplicationMediator mediator = new ApplicationMediator();

        Colleague laptop = new LaptopColleague(mediator);
        Colleague mobile = new MobileColleague(mediator);

        mediator.addColleague(laptop);
        mediator.addColleague(mobile);

        laptop.send("Hello World");
        mobile.send("Hello");

    }
}