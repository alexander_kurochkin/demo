package com.luxoft.patterns.behavioral.state;

/**
 * @author Alexander Kurochkin
 *
 * state machine -controls swiching
 */
public interface BalanceCalculator {

    void approve(Transaction transaction);

    void increaseBalance(Transaction transaction);

}
