package com.luxoft.patterns.behavioral.templateMethod;

/**
 * @author Alexander Kurochkin
 */
public abstract class ServiceTemplate<T> implements Service<T> {
    @Override
    public void init() {
        System.out.println("init");
    }

    public abstract void service(T argument);

    @Override
    public void destroy() {
        System.out.println("destroy");
    }
}
