package com.luxoft.patterns.behavioral.visitor;

/**
 *
 */
public interface Car {

    void accept(Visitor visitor);
}
