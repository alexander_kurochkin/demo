package com.luxoft.patterns.behavioral.Marker;

/**
 * @author Alexander Kurochkin
 * <p>
 * <p>
 * Using empty interfaces as markers to distinguish special treated objects.
 */
public interface Permission {
}
