package com.luxoft.patterns.behavioral.state;

/**
 * @author Alexander Kurochkin
 */
public class InvalidStateException extends RuntimeException {
    private String details;
    private State state;

    public InvalidStateException(String details, State state) {
        this.details = details;
        this.state = state;
    }

    @Override
    public String toString() {
        return "InvalidStateException{" +
                "details='" + details + '\'' +
                ", state=" + state +
                '}';
    }
}
