package com.luxoft.patterns.behavioral.observer;

/**
 * Created by alex on 23.08.2017.
 */
public interface Observable {
    void addObserver(Observer o);

    void removeObserver(Observer o);

    void notifyObservers();
}
