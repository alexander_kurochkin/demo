package com.luxoft.patterns.behavioral.interpreter;

import java.util.Stack;

/**
 * @author Alexander Kurochkin
 */
public class SentenceParser {

    public static void main(String[] args) {

        SentenceParser sentenceParser = new SentenceParser();
        System.out.println(sentenceParser.parse("1 2 -"));
        System.out.println(sentenceParser.parse("4 3 2 - 1 + *"));
    }

    private int parse(String sentence) {

        Stack<Expression> stack = new Stack();

        String[] peases = sentence.split(" ");

        for (String pease : peases) {

            if (isOperator(pease)) {

                Expression right = stack.pop();
                Expression left = stack.pop();

                Expression op = getOperatorInstance(pease, left, right);
                stack.push(new NumberExpression(op.intrerprent()));

            } else {
                stack.push(new NumberExpression(pease));
            }

        }

        return stack.peek().intrerprent();
    }

    private Expression getOperatorInstance(String pease, Expression left, Expression right) {

        switch (pease) {
            case "+":
                return new PlusExpression(left, right);
            case "-":
                return new MinusExpression(left, right);
            case "*":
                return new MultiplyExpression(left, right);
            default:
                throw new IllegalArgumentException("invalid operator : " + pease);
        }

    }

    private boolean isOperator(String pease) {
        return
                "+".equals(pease) ||
                        "-".equals(pease) ||
                        "*".equals(pease)
                ;
    }

}
